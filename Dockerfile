FROM alpine

# install curl and bash
RUN apk add curl bash

# copy crontabs and bash script into container
COPY cronjobs /etc/crontabs/root
COPY updater.sh /root/update.sh
# start crond with log level 8 in foreground, output to stderr
CMD ["crond", "-f", "-d", "8"]
